$("#ex2").slider({});

const sliderSize = 210;

const calcPosition = () => {
    let leftPosition = (parseInt($(`.slider-handle`).eq(0).css(`left`)) / sliderSize) * 100;
    let rightPosition = (parseInt($(`.slider-handle`).eq(1).css(`left`)) / sliderSize) * 100;
    let differ = +$(`#ex2`).attr(`data-slider-max`) - +$(`#ex2`).attr(`data-slider-min`);
    leftPosition = differ / 100 * leftPosition + +$(`#ex2`).attr(`data-slider-min`);
    rightPosition = differ / 100 * rightPosition + +$(`#ex2`).attr(`data-slider-min`);
    return [leftPosition.toFixed(0), rightPosition.toFixed(0)];
};

$(`.slider-handle`).on(`mouseup`, function() {
    let rangePrice = calcPosition();
    $(`#range-price-min`).html(`${rangePrice[0]}`);
    $(`#range-price-max`).html(`${rangePrice[1]}`);
});

let btnAddCards = $(`.add-cards`).eq(0);

let cards = [
    {
        'id': 'card00',
        'imgBackground': '',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '30.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card01',
        'imgBackground': './img/products/furniture/1.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '30.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card02',
        'imgBackground': './img/products/furniture/2.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card03',
        'imgBackground': './img/products/furniture/3.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card04',
        'imgBackground': './img/products/furniture/4.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card05',
        'imgBackground': './img/products/furniture/5.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card06',
        'imgBackground': './img/products/furniture/6.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': true
    },
    {
        'id': 'card07',
        'imgBackground': './img/products/furniture/7.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': true
    },
    {
        'id': 'card08',
        'imgBackground': './img/products/furniture/8.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': true
    },
    {
        'id': 'card09',
        'imgBackground': './img/products/furniture/9.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': true
    },
    {
        'id': 'card10',
        'imgBackground': './img/products/furniture/10.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': false
    },
    {
        'id': 'card11',
        'imgBackground': './img/products/furniture/11.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': false
    },
    {
        'id': 'card12',
        'imgBackground': './img/products/furniture/12.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '30.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': false
    },
    {
        'id': 'card13',
        'imgBackground': './img/products/furniture/13.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': false
    },
    {
        'id': 'card14',
        'imgBackground': './img/products/furniture/14.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': false
    },
    {
        'id': 'card15',
        'imgBackground': './img/products/furniture/15.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '25.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': false
    },
    {
        'id': 'card16',
        'imgBackground': './img/products/furniture/16.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '30.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': false
    },
    {
        'id': 'card17',
        'imgBackground': './img/products/furniture/17.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': false,
        'oldPrice': '$0',
        'onDisp': false
    },
    {
        'id': 'card18',
        'imgBackground': './img/products/furniture/18.png',
        'title': 'Aenean Ru Bristique',
        'rating': '2',
        'price': '15.00',
        'isOldPrice': true,
        'oldPrice': '35.00',
        'onDisp': false
    }
];

const addNewCards = () => {
    let cardCopy;
    let cardColumns = $(`.card-columns`).eq(0);
    let idNoDisp = 1;
    let i = 0;
    while (i < 9) {
        cardCopy = $(`#card00`).eq(0).clone();
        if (!(cards[idNoDisp]['onDisp'])) {
            $(cardCopy).removeClass(`d-none`);
            $(cardCopy).attr('id', cards[idNoDisp]['id']);
            $(cardCopy).find(`.card-img`).eq(0).css(`background-image`, `url("${cards[idNoDisp]['imgBackground']}")`);
            $(cardCopy).find(`.card-title`).eq(0).html(`${cards[idNoDisp]['title']}`);
            $(cardCopy).find(`.card-footer-price`).eq(0).html(`$${cards[idNoDisp]['price']}`);
            if (cards[idNoDisp]['isOldPrice']) {
                $(cardCopy).find(`.old-price`).eq(0).html(`$${cards[idNoDisp]['oldPrice']}`);
                $(cardCopy).find(`.old-price`).eq(0).css(`display`, `block`)
            }
            $(cardColumns).append(cardCopy);
            i++;
        }
        idNoDisp++;
    }
};

const addModal = (event) => {
    let id = $(event.target).closest(`.card`).attr(`id`);
    let obj = {};
    $.each(cards, function () {
        if (this.id === id) {
            obj = this;
        }
    });
    let modalPicture = $(`.modal-content-picture`).eq(0);
    let modalInformation = $(`.modal-content-information`).eq(0);
    $(modalPicture).css(`background-image`, `url("${obj.imgBackground}")`);
    $(modalInformation).find(`.modal-content-title`).eq(0).text(`${obj.title}`);
    if (obj.oldPrice) {
        $(modalInformation).find(`.modal-content-old-price`).eq(0).html(`${obj.oldPrice}`);
    }
    $(modalInformation).find(`.modal-content-price`).eq(0).html(`${obj.price}`);
};

$(btnAddCards).on(`click`, function () {
    addNewCards();
    $(btnAddCards).addClass(`d-none`);
});

$(`.card-img-quick-view`).on(`click`, addModal);

$(`.card-img-quick-view`).on(`click`, function () {
    console.log(event.target)
});

$(`.latest-blog-btn`).on(`click`, function () {
    $(`.latest-blog-btn`).removeClass(`active`);
    $(event.target).addClass(`active`);
    $(`.latest-blog-carousel-indicators`).find(`li`)
        .eq($(`.latest-blog-btn`).index(event.target))
        .trigger(`click`);
});

const changeStylesFocus = () => {
    $(`span.mail`).eq(0).css(`color`, `#d58e32`);
    $(`.news-letter-block`).find(`h4`).eq(0).css(`color`, `#d58e32`);
    $(`span.submit`).eq(0).css(`color`, `#d58e32`);
};

const changeStylesBlur = () => {
    $(`span.mail`).eq(0).css(`color`, `#A9A9A9`);
    $(`.news-letter-block`).find(`h4`).eq(0).css(`color`, `#A9A9A9`);
    $(`span.submit`).eq(0).css(`color`, `#A9A9A9`);
};

$(`#email-input`).on(`focus`, changeStylesFocus);

$(`#email-input`).on(`blur`, changeStylesBlur);